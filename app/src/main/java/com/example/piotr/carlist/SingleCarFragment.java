package com.example.piotr.carlist;

import static android.app.Activity.RESULT_OK;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SingleCarFragment extends Fragment {

    private Car car;
    TextView brand;
    TextView model;
    TextView description;
    ImageView image;

    Button deleteButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View singleCarFragment = inflater.inflate(R.layout.single_car_view, container, false);
        image = (ImageView) singleCarFragment.findViewById(R.id.image_fr);
        brand = (TextView) singleCarFragment.findViewById(R.id.brand_fr);
        model = (TextView) singleCarFragment.findViewById(R.id.model_fr);
        description = (TextView) singleCarFragment.findViewById(R.id.description_fr);

        deleteButton=singleCarFragment.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SingleCarFragment.class);
                intent.putExtra("car_to_delete", car);
                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                getFragmentManager().popBackStack();
            }
        });

        return singleCarFragment;
    }

    @Override
    public void onResume() {

        super.onResume();
        if (getArguments() != null) {
            car = (Car) getArguments().get("car");
            if (car != null) {
                image.setImageResource(car.getImage());
                brand.setText(car.getBrand());
                model.setText(car.getModel());
                description.setText(car.getDescription());
            }
        }
    }
}
