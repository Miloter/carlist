package com.example.piotr.carlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.MyViewHolder> {

    private List<Car> listOfItems;

    Context context;

    private final OnItemClickListener onItemClickListener;

    public MyListAdapter(List<Car> listOfItems, OnItemClickListener onItemClickListener) {
        this.listOfItems = listOfItems;
        this.onItemClickListener = onItemClickListener;
    }

    public MyListAdapter(OnItemClickListener onItemClickListener, Context context) {
        listOfItems = new LinkedList<>();
        this.context = context;
        this.onItemClickListener = onItemClickListener;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View singleItemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_list_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(singleItemLayout);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.bind(listOfItems.get(position),onItemClickListener);
    }


    @Override
    public int getItemCount() {
        return listOfItems.size();
    }

     public interface OnItemClickListener {
        void onItemClick(Car item);
    }



    public void setListOfItems(List<Car> listOfItems){
        this.listOfItems=listOfItems;
    }
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textToView;

        public TextView brand, model;
        public ImageView image;
        private LinearLayout main;

        Car car;

        public MyViewHolder(View itemView) {
            super(itemView);
            model = (TextView) itemView.findViewById(R.id.model);
            brand = (TextView) itemView.findViewById(R.id.brand);
            image = (ImageView) itemView.findViewById(R.id.image);
            main = (LinearLayout) itemView.findViewById(R.id.main);

        }
        void bind(final Car car , final OnItemClickListener  onItemClickListener){

            this.car=car;
            brand.setText(car.getBrand());
            model.setText(car.getModel());
            image.setImageResource(car.getImage());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(car);
                }
            });

        }
    }
}
