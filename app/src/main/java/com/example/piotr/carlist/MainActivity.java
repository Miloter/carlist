package com.example.piotr.carlist;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Car> carList;

    private MyListAdapter myListAdapter;

    RecyclerView recyclerView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState != null && savedInstanceState.containsKey("carList")) {
            Car[] carArray = (Car[]) savedInstanceState.getParcelableArray("carList");
            runCarListFragment(new ArrayList<Car>(Arrays.asList(carArray)));
            return;
        }
        runCarListFragment(null);

    }


    private void runCarListFragment(List<Car> listOfCars) {

        if (listOfCars == null) {
            carList = prepareItem();
        } else {
            carList = listOfCars;
        }
        CarListFragment carListFragment = new CarListFragment();
        carListFragment.setCarList(carList);
        getFragmentManager().beginTransaction().replace(R.id.recycler_view_list, carListFragment).commit();
    }

    public List<Car> prepareItem() {
        Context ctx=getApplicationContext();

        ctx.getResources().openRawResource(R.raw.dane);

        List<Car> carListFromFile = new ArrayList<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(ctx.getResources().openRawResource(R.raw.dane)));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] splitedLine= line.split(";");
                carListFromFile.add(new Car(splitedLine[0],splitedLine[1],splitedLine[2],R.drawable.car));
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return carListFromFile;

    }

    void removeCar(Car car) {
        Iterator<Car> carIterator = carList.iterator();
        while (carIterator.hasNext()) {
            Car car1 = carIterator.next();
            if (car1.getBrand().equals(car.getBrand()) && car1.getModel().equals(car.getModel())) {
                carIterator.remove();
            }
        }
    }

}


