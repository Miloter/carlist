package com.example.piotr.carlist;

import android.os.Parcel;
import android.os.Parcelable;

public class Car implements Parcelable {

    private String brand;
    private String model;
    private String description;
    private int image;

    public Car(String brand, String model, String description, int image){
        this.brand=brand;
        this.model=model;
        this.description=description;
        this.image=image;
    }

    protected Car(Parcel in) {
        brand = in.readString();
        model = in.readString();
        description = in.readString();
        image = in.readInt();
    }

    public static final Creator<Car> CREATOR = new Creator<Car>() {
        @Override
        public Car createFromParcel(Parcel in) {
            return new Car(in);
        }

        @Override
        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(brand);
        dest.writeString(model);
        dest.writeString(description);
        dest.writeInt(image);
    }
}
