package com.example.piotr.carlist;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CarListFragment extends Fragment implements MyListAdapter.OnItemClickListener{

    List<Car> carList;

    private MyListAdapter myListAdapter;

    RecyclerView recyclerView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View rootView = inflater.inflate(R.layout.recycler_view, container, false);
        recyclerView = rootView.findViewById(R.id.rv_show_list);
        return rootView;

/*
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        myListAdapter = new MyListAdapter(carList);

        // RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setAdapter(myListAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
*/



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        myListAdapter = new MyListAdapter( this,getContext());
        recyclerView.setAdapter(myListAdapter);
        myListAdapter.setListOfItems(carList);
    }
    @Override
    public void onItemClick(Car item) {

        if(getActivity().getFragmentManager().findFragmentById(R.id.single_car)!=null){
            getActivity().getFragmentManager().popBackStack();
        }
        SingleCarFragment singleCarFragment= new SingleCarFragment();
        singleCarFragment.setTargetFragment(CarListFragment.this,getTargetRequestCode());
        Bundle bundle = new Bundle();
        bundle.putParcelable("car",item);
        singleCarFragment.setArguments(bundle);
        getActivity().getFragmentManager().beginTransaction().replace(R.id.single_car,singleCarFragment).addToBackStack(null).commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if(resultCode == RESULT_OK){
            Car car = data.getParcelableExtra("car_to_delete");
            Iterator<Car> carIterator= carList.iterator();
            while(carIterator.hasNext()){
                Car car1=carIterator.next();
                if(car1.getBrand().equals(car.getBrand())&& car1.getModel().equals(car.getModel())){
                    carIterator.remove();
                }
            }

            myListAdapter.notifyDataSetChanged();
            ((MainActivity) getActivity()).removeCar(car);
        }

    }
/*
    private void setRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }*/

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }


    /*    public void prepareItem(){
        carList.add(new Car("Audi", "A4", "Całkiem fajny", R.drawable.car));
        carList.add(new Car("Mercedes", "CLS", "Całkiem fajny", R.drawable.car));
        carList.add(new Car("Audi", "A5", "Całkiem fajny", R.drawable.car));
        carList.add(new Car("Mercedes", "S", "Całkiem fajny", R.drawable.car));
        carList.add(new Car("Audi", "A6", "Całkiem fajny", R.drawable.car));
        carList.add(new Car("Mercedes", "E", "Całkiem fajny", R.drawable.car));
        carList.add(new Car("Audi", "A7", "Całkiem fajny", R.drawable.car));
        carList.add(new Car("Mercedes", "C", "Całkiem fajny", R.drawable.car));
        myListAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(myListAdapter);
    }*/
}
