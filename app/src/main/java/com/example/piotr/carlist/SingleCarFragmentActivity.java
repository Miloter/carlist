package com.example.piotr.carlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class SingleCarFragmentActivity extends FragmentActivity {

    TextView  brand;
    TextView  model;
    TextView  description;
    ImageView image;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_car_activity);

        image = (ImageView) findViewById(R.id.image_act);
        brand = (TextView) findViewById(R.id.brand_act);
        model = (TextView) findViewById(R.id.model_act);
        description = (TextView) findViewById(R.id.description_act);

        Intent intent = getIntent();
        Car car = intent.getParcelableExtra("car");
        setTitle(car.getBrand());
        image.setImageResource(car.getImage());
        brand.setText(car.getBrand());
        model.setText(car.getModel());
        description.setText(car.getDescription());
    }
}

